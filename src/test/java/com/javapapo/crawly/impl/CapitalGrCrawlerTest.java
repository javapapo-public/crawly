package com.javapapo.crawly.impl;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.config.NewsPaper;
import com.javapapo.crawly.config.NewsPaperSection;
import com.javapapo.crawly.crawlers.impl.greek.CapitalGrCrawler;
import java.nio.file.Path;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public class CapitalGrCrawlerTest extends  BaseTest{

    private CapitalGrCrawler crawler =null;

    private static String testLink ="https://www.capital.gr/diethni/3361995/dw-pio-pithano-pleon-to-senario-sklirou-brexit";

    @Before
    public void init(){
      ConfigModel configModel = new ConfigModel();
      NewsPaper newspaper = new NewsPaper();
      newspaper.setName("capitalgr");
      newspaper.setType("greek");
      NewsPaperSection section = new NewsPaperSection();
      section.setLink("xxx");
      section.setName("xxx");
      newspaper.setSections(List.of(section));
      configModel.setNewspapers(List.of(newspaper));
      crawler = new CapitalGrCrawler(client,configModel, Path.of(".","crawlyreports"),5);
    }

    @Test
    public void getArticleContent(){
        String result=null;
        try {
            result= crawler.getArticleContent(testLink);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertTrue("Article should not be null",result!=null);
        Assert.assertTrue("Article should not be empty",!result.isEmpty());
        Assert.assertTrue(result.startsWith("<div><h2>DW: Πιο πιθανό"));
    }
}
