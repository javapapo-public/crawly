package com.javapapo.crawly.impl;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.config.NewsPaper;
import com.javapapo.crawly.config.NewsPaperSection;
import com.javapapo.crawly.crawlers.impl.greek.ProtoThemaCrawler;
import java.nio.file.Path;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public class PrototoThemaCrawlerTest extends BaseTest{
    ProtoThemaCrawler crawler =null;

    private static String testLink ="http://www.protothema.gr//economy/article/536826/dombrovskis-evaluation-to-be-completed-after-mid-february-says/";

    @Before
    public void init(){
      ConfigModel configModel = new ConfigModel();
      NewsPaper newspaper = new NewsPaper();
      newspaper.setName("thepressproject");
      newspaper.setType("greek");
      NewsPaperSection section = new NewsPaperSection();
      section.setLink("xxx");
      section.setName("xxx");
      newspaper.setSections(List.of(section));
        crawler = new ProtoThemaCrawler(client,configModel, Path.of(".","crawlyreports"),5);
    }

    @Test
    public void getArticleContent(){
        String result=null;
        try {
            result= crawler.getArticleContent(testLink);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(result);
        Assert.assertTrue("Article should not be null",result!=null);
        Assert.assertTrue("Article should not be empty",!result.isEmpty());
        Assert.assertTrue(result.startsWith("<h2>Dombrovskis: Evaluation "));
    }
}
