//package com.javapapo.crawly.impl;
//
//import com.javapapo.crawly.crawlers.impl.internal.greek.VimaCrawler;
//import java.util.Map;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
///**
// * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
// */
//public class VimaCrawlerTest extends BaseTest{
//
//    VimaCrawler crawler =null;
//
//    private static String testLink ="http://www.tovima.gr/society/article/?aid=762422";
//
//    @Before
//    public void init(){
//        crawler = new VimaCrawler(client);
//    }
//
//    @Test
//    public void getSections() {
//        Map<String,String> sections=crawler.getSupportedSections();
//        Assert.assertTrue("Sections should not be empty",!sections.isEmpty());
//    }
//
//
//    @Test
//    public void getArticleContent(){
//        String result=null;
//        try {
//            result= crawler.getArticleContent(testLink);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println(result);
//        Assert.assertTrue("Article should not be null",result!=null);
//        Assert.assertTrue("Article should not be empty",!result.isEmpty());
//        Assert.assertTrue(result.startsWith("<h2>Δράση για μετανάστευση"));
//    }
//
//    /**
//     * The reason for this test, is to have a quick way to actually
//     * invoke the crawler and check it's overall functionality. There
//     * are several things that might go wrong during crawling, I am mostly
//     * interested on exceptions from libs used (e.g jsoup).
//     * The newspapers often change their DOM too often and many things could go wrong.
//     */
//    @Test
//    public void testCrawl(){
//        boolean exceptionThrown = false;
//        try {
//            crawler.call();
//        } catch (Exception e) {
//            exceptionThrown=true;
//            e.printStackTrace();
//        }
//        if(exceptionThrown){
//            Assert.fail();
//        }
//    }
//
//
//}
