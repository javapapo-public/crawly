package com.javapapo.crawly.impl;

import com.javapapo.crawly.config.ConfigModel;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpClient.Version;
import java.time.Duration;
import java.util.List;
import org.junit.BeforeClass;

public class BaseTest {

  static HttpClient client;
  static ConfigModel configModel;

  @BeforeClass
  public static void  before(){
    client =  HttpClient.newBuilder()
        .version(Version.HTTP_1_1)
        .followRedirects(Redirect.NORMAL)
        .connectTimeout(Duration.ofSeconds(20))
        .build();
  }

}
