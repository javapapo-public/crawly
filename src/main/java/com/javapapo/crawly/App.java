package com.javapapo.crawly;

import com.javapapo.crawly.cmdline.CrawlyCmdArgs;
import com.javapapo.crawly.cmdline.CrawlyCmdParamsEnum;
import com.javapapo.crawly.cmdline.CrawlyCmdUtil;
import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.config.NewsPaper;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.utils.GmailUtil;
import com.javapapo.crawly.utils.ZipUtil;
import java.io.IOException;
import java.io.InputStream;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Redirect;
import java.net.http.HttpClient.Version;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 * Main App
 * <p>
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 08/12/15.
 */
@Slf4j
public class App {

  private static final HttpClient client = HttpClient.newBuilder()
      .version(Version.HTTP_1_1)
      .followRedirects(Redirect.NORMAL)
      .connectTimeout(Duration.ofSeconds(20))
      .build();
  private static ExecutorService executor;
  private static final ConfigModel config = readConfig();

  public static void main(String[] args) throws Exception {

    CrawlyCmdArgs cmdArgs = CrawlyCmdUtil.parseArguments(args);
    if (Objects.nonNull(cmdArgs)) {
      //proxy setting if set.
      setProxy(cmdArgs);
      //where to save the files
      Path outputDir = createDirectory(cmdArgs.getBaseDir());
      //loop the impl - instantiate and configure the crawlers
      List<Crawly> crawlies = doConfigureCrawlers(cmdArgs, outputDir);

      if (!crawlies.isEmpty()) {
        Instant before = Instant.now();
        if (cmdArgs.isDoParallel()) {
          doParallel(crawlies);
        }
        else {
          crawlies.forEach(Crawly::call);
        }
        Instant after = Instant.now();
        boolean doZip = cmdArgs.isPerformZip();
        if(cmdArgs.isSendEmail()){
          doZip=true;
        }

        if (doZip) {
          Path zipFile =ZipUtil.zipFile(cmdArgs.getBaseDir(), outputDir.toFile());
          if(zipFile!=null){
            if(cmdArgs.isSendEmail()) {
              GmailUtil.sendEmail(config.getEmail(), cmdArgs.getSendEmailTo(),cmdArgs.getGmailUsername(),
                  cmdArgs.getGmailPassword(),zipFile.toFile());
            }
          }
        }
        log.info("Total Crawling time was :" + Duration.between(before, after).getSeconds() + " seconds");
      } else {
        log.warn("You have not provided any language type, no crawlers will run, please use a specific " +
            "flag e.g -english to activate english lang crawlers");
      }
    }

  }

  private static ConfigModel readConfig() {
    Yaml yaml = new Yaml(new Constructor(ConfigModel.class));
    InputStream inputStream = App.class.getClassLoader()
        .getResourceAsStream("application.yaml");
    return (ConfigModel) yaml.load(inputStream);
  }

  /**
   * Loads the class definition of crawlers and based on the command line arguments, initializes them accordingly
   * Returns a List with initialized instances ready to run in sequential or paralle mode.
   *
   * @param cmdArgs Cmd args of the program
   */
  private static List<Crawly> doConfigureCrawlers(CrawlyCmdArgs cmdArgs, Path outputDir) throws Exception {
    List<Crawly> configuredCrawlers = new ArrayList<>();
    List<NewsPaper> sites = config.getNewspapers();
    for (NewsPaper site : sites) {
      if (site.isEnabled()) {
        Class c = Class.forName(site.getClassName());
        Crawly instance = (Crawly) c
            .getDeclaredConstructor(HttpClient.class, ConfigModel.class, Path.class, Integer.class)
            .newInstance(client, config, outputDir, cmdArgs.getMaxItems());
        CrawlerLangTypesEnum type = instance.getLangType();
        if (cmdArgs.isDoGreek() && CrawlerLangTypesEnum.greek.equals(type)) {
          configuredCrawlers.add(instance);
        }
      }
    }

    return configuredCrawlers;
  }

  private static Path createDirectory(String baseDir) throws IOException {
    Path baseDirPath = Paths.get(
        baseDir + "/" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
    if (!Files.exists(baseDirPath)) {
      Files.createDirectory(baseDirPath);
    }
    return baseDirPath;
  }

  /**
   * In case the user has provided proxy and port settings, apply tsystem variable
   *
   * @param cmdArgs Command line arguments pojo
   */
  private static void setProxy(CrawlyCmdArgs cmdArgs) {
    if (Objects.nonNull(cmdArgs.getProxy())
        && !cmdArgs.getProxy().isEmpty()
        && Objects.nonNull(cmdArgs.getProxyPort())) {
      System.setProperty(CrawlyCmdParamsEnum.httpProxyHost.literal(), cmdArgs.getProxy());
      System.setProperty(CrawlyCmdParamsEnum.httpProxyPort.literal(), cmdArgs.getProxyPort());
      log.info("Proxy : " + cmdArgs.getProxy() + " and Port: " + cmdArgs.getProxy() + " were set");
    }
  }

  /**
   * Initialize a fixed thread executor see {@linkplain ExecutorService} and then uses the list of cralwers that support
   * the {@linkplain Callable} interface, to run them in parallel mode
   *
   * @param crawlies List<Crawly>
   */
  private static void doParallel(List<Crawly> crawlies) {
    log.info("Parallel crawling enabled available processors :" + Runtime.getRuntime().availableProcessors());
    List<Callable<String>> callableCrawlers = new ArrayList<>(crawlies);
    executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    try {
      executor.invokeAll(callableCrawlers);
    }
    catch (InterruptedException e) {
      log.warn("The parallel execution has encountered a problem- re-run the app!");
    }
    finally {
      executor.shutdown();
    }
  }

}
