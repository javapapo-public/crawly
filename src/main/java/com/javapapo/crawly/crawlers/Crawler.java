package com.javapapo.crawly.crawlers;

import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;

/**
 * Interface defining all the  methods that a crawly 'crawler' should implement / feature.
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public interface Crawler {

    /**
     * @return The name of the site / news paper
     */
    String getSiteName();

    /**
     * Each crawler supports a lang type
     * @return CrawlerLangTypesEnum
     */
    CrawlerLangTypesEnum getLangType();

    /**
     * @return The url of the site / news paper
     */
    String getSiteUrl();

    /**
     * Each crawly needs to provide the logic where the framework logic (CSS selector like) where the
     * engine will be able to get the part of the overall DOM document, that corresponds to the article text
     * @return Function<Document,Element>
     */
    ArticleBodyParser provideArticleBodyParser();

}
