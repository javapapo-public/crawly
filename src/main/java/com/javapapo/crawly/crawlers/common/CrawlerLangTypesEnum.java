package com.javapapo.crawly.crawlers.common;

/**
 * Types (languages) for different crawlers. Each crawler is bound to one
 * of this types.
 * Created by javapapo on 29/12/15.
 */
public enum CrawlerLangTypesEnum {

    english,
    german,
    greek
}
