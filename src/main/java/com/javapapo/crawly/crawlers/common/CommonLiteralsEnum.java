package com.javapapo.crawly.crawlers.common;

/**
 * Common literals used in Cralwers in the form of an enum
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 14/12/15.
 */
public enum CommonLiteralsEnum {

    SKAIGR_ΝΑΜΕ("skaigr"),
    SKAIGR_MAIN_URL("http://www.skai.gr/"),

    PARAPOLITIKA_ΝΑΜΕ("parapolitika"),
    PARAPOLITIKA_MAIN_URL("http://www.parapolitika.gr/"),


    PROTAGON_ΝΑΜΕ("protagon"),
    PROTAGON_MAIN_URL("http://www.protagon.gr/"),

    INDEPENDENT_ΝΑΜΕ("independent"),
    INDEPENDENT_MAIN_URL("http://www.independent.co.uk"),

    INGR_ΝΑΜΕ("InGr"),
    INGR_MAIN_URL("http://www.in.gr/"),

    VIMA_NAME("vima"),
    VIMA_MAIN_URL("http://www.tovima.gr"),

    REALNEWS_NAME("realnews"),
    REALNEWS_MAIN_URL("http://www.real.gr/"),

    PROTOTHEMA_NAME("protothema"),
    PROTOTHEMA_MAIN_URL("http://www.protothema.gr"),

    //nafteboriki
    NAFTEBORIKI_NAME("naftemporiki"),
    NAFTEBORIKI_MAIN_URL("http://www.naftemporiki.gr/"),

    //nafteboriki
    PRESSPROJECT_NAME("thepressproject"),
    PRESSPROJECT_MAIN_URL("http://www.thepressproject.gr"),

    //nafteboriki
    CAPITAL_NAME("capitalgr"),
    CAPITAL_MAIN_URL("http://www.capital.gr/"),

    //sports24
    SPORT24_NAME("Sport24"),
    SPORT24_MAIN_URL("http://www.sport24.gr/"),

    //in.gr
    CONTRAGR_NAME("ContraGr"),
    CONTRAGR_MAIN_URL("http://www.contra.gr/"),

    //ta nea
    TANEA_ΝΑΜΕ("tanea"),
    TANEA_MAIN_URL("http://www.tanea.gr/"),

    //kathimerini
    KATHIMERINI_NAME("Kathimerini"),
    KATHIMERINI_MAIN_URL("http://www.kathimerini.gr/")

    ;

    private final String literal;

    CommonLiteralsEnum(String aLit){
        this.literal = aLit;
    }

    public String getValue(){
        return  this.literal;
    }
}
