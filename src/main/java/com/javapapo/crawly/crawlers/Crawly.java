package com.javapapo.crawly.crawlers;

import static com.javapapo.crawly.utils.HtmlContentWriterUtil.printSection;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.config.NewsPaper;
import com.javapapo.crawly.config.NewsPaperSection;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.utils.HtmlContentWriterUtil;
import com.javapapo.crawly.utils.JSoupUtils;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import lombok.extern.slf4j.Slf4j;

/**
 * Basic Crawler functionality acting as a small template of actions, delegating specific functionality to subclasses.
 * Contains the core logic and orchastration of actions. Created by <a href="mailto:javapapo@mac.com">javapapo</a> on
 * 14/12/15.
 */
@Slf4j
public abstract class Crawly implements Crawler, Callable<String> {

  private static final String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
  private static final String ampLiteral = "amp;";

  private int maxItems = 5;
  private Path baseDir;
  private HttpClient client;
  private ConfigModel configModel;

  public Crawly(HttpClient client, ConfigModel configModel,Path baseDir,Integer maxItems){
    this.client= client;
    this.configModel = configModel;
    this.baseDir = baseDir;
    this.maxItems = maxItems;
  }

  /**
   * To be invoked by an executor or standalone call. Starts the crawling.
   *
   * @return String status message
   */
  @Override
  public String call() {
    String result = getSiteName() + "Crawler" + " completed";
    try {
      this.startCrawl();
    }
    catch (Exception e) {
      log.warn("Exception was thrown during crawling ");
      result = "An error occured" + e.getMessage();
    }
    return result;
  }

  /**
   * The template method
   */
  private void startCrawl() throws Exception {
    printStartCrawling();
    //a list of buffers to contain html- each section contains several articles crawled
    List<StringBuilder> buffers = new ArrayList<>();
    //loop through the sections
    List<NewsPaperSection>  sections = getSupportedSections();
    for(NewsPaperSection section: sections) {
      //find the article links each section
      TreeMap allLInks = gatherLinks(section.getLink());
      List<String> links = limitLinks(allLInks);
      if (!links.isEmpty()) {
        StringBuilder sectionBuffer = new StringBuilder();
        sectionBuffer.append(printSection(section.getName()));
        //add the article to the main buffer within a div
        links.forEach((k) -> sectionBuffer.append(getArticleContent(k)));
        buffers.add(sectionBuffer);
      }
    }
    //generate content
    StringBuilder htmlContent = HtmlContentWriterUtil.generateHtml(getSiteName(), buffers);

    //save to file
    if (htmlContent != null) {
      File savedReport = saveFile(getSiteName(), htmlContent);
      printEndCrawling(false);
    }
    else {
      printEndCrawling(true);
    }
  }

  /**
   * Gets the content of the article, and creates the final section for the generated report (html)
   */
  public String getArticleContent(String aLink) {

    HttpRequest req = HttpRequest.newBuilder().GET().uri(URI.create(aLink))
        .header("User-Agent",userAgent)
        .build();
    String responseBody=null;
    try {
      responseBody= client.send(req, BodyHandlers.ofString()).body();
    }
    catch (Exception e) {
      log.error("We could not fetch the article with link; {}", aLink,e);
    }
    return JSoupUtils.getArticleContent(aLink, responseBody,provideArticleBodyParser());
  }

  private SyndFeed getSectionLinksFromRssFeed(String sectionLink) {
    SyndFeed feed = null;
    try {
      HttpResponse<String> response = client.send(HttpRequest.newBuilder().GET()
          .header("User-Agent",userAgent)
          .uri(new URI(sectionLink)).build(),BodyHandlers.ofString());
      var input = new SyndFeedInput();
      feed = input.build(new StringReader(response.body()));
    }
    catch (Exception e) {
      log.warn("Unable to fetch the rss feed for  url: {} and exception: {}", sectionLink, e.getMessage());
    }
    return feed;
  }

  private TreeMap gatherLinks(String sectionLink) {
    TreeMap linkMap = new TreeMap(Collections.reverseOrder());
    //for sites that support rss we do rss
    SyndFeed feed = getSectionLinksFromRssFeed(sectionLink);
    if (feed != null) {
      for (SyndEntry anEntry : feed.getEntries()) {
        String aLink = anEntry.getLink();
        if (aLink.contains(ampLiteral)) {
          aLink = aLink.replace(ampLiteral, "");
        }
        if (!shoudRemove(anEntry)) {
          linkMap.put(aLink, aLink);
        }
      }
    }
    //for sites that do not have or do not support
    return linkMap;
  }

  /**
   * Check the syndentry for creation or update date. If they are available (in some feeds they are not!) then perform a
   * simple check - if the article was posted today, or updated today.
   *
   * @param anEntry The entry of feed entry
   * @return boolean
   */
  private boolean shoudRemove(SyndEntry anEntry) {
    boolean shouldRemove = false;
    if (Objects.nonNull(anEntry.getPublishedDate())) {
      Date pubD = anEntry.getPublishedDate();
      LocalDate publishLocalDate =
          LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(pubD));
      if (!LocalDate.now().equals(publishLocalDate)) {
        shouldRemove = true;
      }
    }
    return shouldRemove;
  }

  /**
   * Given a TreeMap containing links per section, applies a limit (based on the configuration) and returns a new list
   * with the requested link size
   */
  private List<String> limitLinks(TreeMap<String, String> linkMap) {
    List<String> result = new ArrayList<>();
    if (linkMap != null && !linkMap.isEmpty()) {
      for (Map.Entry<String, String> entry : linkMap.entrySet()) {
        String aLink = entry.getValue();
        if (result.size() < maxItems) {
          result.add(aLink);
        }
        else {
          break;
        }
      }
    }
    return result;
  }

  /**
   * Saves the crawled content to an html file
   *
   * @param newsPaperName The name of the newspaper
   * @param htmlContent A String builder containing all the contents of the generated html report
   */
  private File saveFile(String newsPaperName, StringBuilder htmlContent) throws IOException {
    String aFileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    Path aFile = Paths.get(this.baseDir.toString(), newsPaperName + "-" + aFileDate + ".html");
    if (Objects.nonNull(htmlContent)) {
      try (BufferedWriter out = Files.newBufferedWriter(aFile, StandardCharsets.UTF_8)) {
        out.write(htmlContent.toString());
      }
    }
    else {
      log.warn("WARNING : No html content for the report is going to be empty for site: " + newsPaperName);
    }
    return aFile.toFile();
  }

  public List<NewsPaperSection>  getSupportedSections() {
    NewsPaper config =
        this.configModel.getNewspapers().stream().filter(newspaper -> newspaper.getName().equalsIgnoreCase(this.getSiteName()))
            .findFirst().orElseThrow(IllegalStateException::new);
    return config.getSections();

  }

  private void printStartCrawling() {
    String msg = " ******** START CRAWL for " +
        getSiteName().toUpperCase() + " (" + getSiteUrl() + ") ********";
    log.info(msg);
  }

  private void printEndCrawling(boolean wasEmpty) {
    String msg = " ******** END OF CRAWL for " + getSiteName().toUpperCase() + " ********";
    if (!wasEmpty) {
      log.info(msg);
    } else {
      log.info(msg + " NO CONTENT GATHERED");
    }

  }

  public abstract CrawlerLangTypesEnum getLangType();

}
