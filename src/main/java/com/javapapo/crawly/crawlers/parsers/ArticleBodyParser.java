package com.javapapo.crawly.crawlers.parsers;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Functional interface for the parsing of the html article
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 22/12/15.
 */
@FunctionalInterface
public interface ArticleBodyParser {

    /**
     * Receives a jsoup Document and provides the logic to identify and parse the Element of the JSOUP document
     * that contains the main body of the article;
     * @param aDocument A
     * @return Element,jsoup e.g 1233
     */
    Element parseArticleFromDoc(Document aDocument);
}
