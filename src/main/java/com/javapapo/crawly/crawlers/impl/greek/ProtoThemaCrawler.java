package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * CrawlyApp implementation for <a href="http://www.protothema.gr">Protothema.gr</a>
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 14/12/15.
 */
public class ProtoThemaCrawler extends Crawly implements Crawler {

    public ProtoThemaCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    public String getSiteName() {
        return CommonLiteralsEnum.PROTOTHEMA_NAME.getValue();
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.PROTOTHEMA_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByClass("article-content").first();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

}
