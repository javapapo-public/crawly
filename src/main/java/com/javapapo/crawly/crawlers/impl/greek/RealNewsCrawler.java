package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * Crawler for realnews.gr
 * Created by javapapo on 02/01/16.
 */
public class RealNewsCrawler extends Crawly implements Crawler {

    public RealNewsCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    @Override
    public String getSiteName() {
        return CommonLiteralsEnum.REALNEWS_NAME.getValue();
    }

    @Override
    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

    @Override
    public String getSiteUrl() {
        return CommonLiteralsEnum.REALNEWS_MAIN_URL.getValue();
    }

    @Override
    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByClass("entry-content").first();
    }
}
