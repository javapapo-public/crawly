package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * Crawler for Greek  news site www.capital.gr
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public class CapitalGrCrawler extends Crawly implements Crawler {

    public CapitalGrCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    public String getSiteName() {
        return CommonLiteralsEnum.CAPITAL_NAME.getValue();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.CAPITAL_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementById("articleBody");
    }
}