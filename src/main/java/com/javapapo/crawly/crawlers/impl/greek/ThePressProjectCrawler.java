package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * Crawler for the www.thepressproject.gr
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public class ThePressProjectCrawler extends Crawly implements Crawler {

    public ThePressProjectCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir, maxItems);
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementById("maintext");
    }

    public String getSiteName() {
        return CommonLiteralsEnum.PRESSPROJECT_NAME.getValue();
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.PRESSPROJECT_MAIN_URL.getValue();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }


}
