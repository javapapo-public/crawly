package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * Concrete crawler for the site www.skai.gr
 * Created by javapapo on 15/01/16.
 */
public class SkaiGrCrawler extends Crawly implements Crawler {

    public SkaiGrCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir, maxItems);
    }

    @Override
    public String getSiteName() {
        return CommonLiteralsEnum.SKAIGR_ΝΑΜΕ.getValue();
    }

    @Override
    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

    @Override
    public String getSiteUrl() {
        return CommonLiteralsEnum.SKAIGR_ΝΑΜΕ.getValue();
    }

    @Override
    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByTag("article").first();
    }

}
