package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;
import org.jsoup.nodes.Element;

/**
 * CrawlyApp implementation for <a href="http://www.naftemporiki.gr">Naftemporiki.gr</a>
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 15/12/15.
 */
public class NafteborikiCrawler extends Crawly implements Crawler {

    public NafteborikiCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    public String getSiteName() {
        return CommonLiteralsEnum.NAFTEBORIKI_NAME.getValue();
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.NAFTEBORIKI_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> {
            Element div = doc.getElementsByClass("entityMain").first();
            return div.getElementById("spBody");
        };
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

}
