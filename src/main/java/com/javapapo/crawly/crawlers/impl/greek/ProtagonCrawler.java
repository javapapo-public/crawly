package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * Concrete Crawler for the site protagon.gr
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 21/12/15.
 */
public class ProtagonCrawler extends Crawly implements Crawler {

    public ProtagonCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel, outputDir,maxItems);
    }

    public String getSiteName() {
        return "protagon";
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.PROTAGON_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByClass("content_space").first();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

}


