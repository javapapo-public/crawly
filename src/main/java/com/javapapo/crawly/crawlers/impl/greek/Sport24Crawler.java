package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * CrawlyApp for Greek sports news site www.sports24.gr
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 15/12/15.
 */
public class Sport24Crawler extends Crawly implements Crawler {

    public Sport24Crawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    public String getSiteName() {
        return CommonLiteralsEnum.SPORT24_NAME.getValue();
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.SPORT24_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByClass("body").first();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }


}
