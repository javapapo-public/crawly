package com.javapapo.crawly.crawlers.impl.greek;

import com.javapapo.crawly.config.ConfigModel;
import com.javapapo.crawly.crawlers.common.CommonLiteralsEnum;
import com.javapapo.crawly.crawlers.common.CrawlerLangTypesEnum;
import com.javapapo.crawly.crawlers.Crawler;
import com.javapapo.crawly.crawlers.Crawly;
import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.net.http.HttpClient;
import java.nio.file.Path;

/**
 * CrawlyApp for Greek sports news site www.contra.gr
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
public class ContraGrCrawler extends Crawly implements Crawler {

    public ContraGrCrawler(HttpClient client, ConfigModel configModel, Path outputDir,Integer maxItems) {
        super(client,configModel,outputDir,maxItems);
    }

    public String getSiteName() {
        return CommonLiteralsEnum.CONTRAGR_NAME.getValue();
    }

    public CrawlerLangTypesEnum getLangType() {
        return CrawlerLangTypesEnum.greek;
    }

    public String getSiteUrl() {
        return CommonLiteralsEnum.CONTRAGR_MAIN_URL.getValue();
    }

    public ArticleBodyParser provideArticleBodyParser() {
        return (doc) -> doc.getElementsByClass("body").first();
    }

}
