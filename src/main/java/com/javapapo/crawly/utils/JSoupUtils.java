package com.javapapo.crawly.utils;

import com.javapapo.crawly.crawlers.parsers.ArticleBodyParser;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;

/**
 * Utility class to contain most of the JSOUP specific code Created by <a href="mailto:javapapo@mac.com">javapapo</a> on
 * 15/01/16.
 */
@Slf4j
public class JSoupUtils {

  /**
   * Gets the content of the article, and creates the final section for the generated report (html)
   */
  public static String getArticleContent(String aLink,String htmlBody, ArticleBodyParser parser) {
    Document doc;
    String htmltext = "";
    if (Objects.nonNull(htmlBody)) {
      doc = Jsoup.parseBodyFragment(htmlBody);
      doc.select("script, style, .hidden").remove();
      Element title = doc.getElementsByTag("title").first();
      Element mainArticle = parser.parseArticleFromDoc(doc);
      if (Objects.nonNull(mainArticle)) {
        htmltext = HtmlContentWriterUtil.generateArticleHtml(title.html(), aLink, Jsoup.clean(mainArticle.html(), Whitelist.basic()));
      } else {
        log.warn("We could not fetch the main body for link: " + aLink);
      }
    }
    return htmltext;
  }
}
