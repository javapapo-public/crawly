package com.javapapo.crawly.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import lombok.extern.slf4j.Slf4j;

/**
 * Used for zip of html files Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 16/12/15.
 */
@Slf4j
public class ZipUtil {

  /**
   * Gets a File and creates on the same directory with the same level a new ziped version. For example if you pass
   * C:\test.html it will create a C:\test.html.zip
   *
   * @param aDirectoryToZip The dir to create a new zip copy from
   * @return The zipped File or null in case of any error.
   */
  public static Path zipFile(String baseDir, File aDirectoryToZip) {
    Path result = null;
    try {
      result = Files.createFile(Paths.get(baseDir, aDirectoryToZip.getName() + ".zip"));
      Path pp = aDirectoryToZip.toPath();
      try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(result));
          Stream<Path> paths = Files.walk(pp)) {
        paths
            .filter(path -> !Files.isDirectory(path))
            .forEach(path -> {
              ZipEntry zipEntry = new ZipEntry(pp.relativize(path).toString());
              try {
                zs.putNextEntry(zipEntry);
                Files.copy(path, zs);
                zs.closeEntry();
              }
              catch (IOException e) {
                log.error("Error during ziping file", e);
              }
            });
        return result;
      }
    }
    catch (Exception e) {
      log.error("Error during ziping file", e);
    }
    return result;
  }

}
