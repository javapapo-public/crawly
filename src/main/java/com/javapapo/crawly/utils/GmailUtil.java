package com.javapapo.crawly.utils;

import com.javapapo.crawly.config.EmailConfig;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class GmailUtil {

  private static final String SUBJECT = "Crawly news for " + LocalDate.now().format(DateTimeFormatter.ISO_DATE);
  private static final String MSG = "The news for today";

  public static void sendEmail(EmailConfig config, String sendToEmail, String maiUsername, String mailPassword,  File zipFile) {
    String to = sendToEmail;
    String from = maiUsername+"@gmail.com";
    final String username = maiUsername;
    final String password = mailPassword;
    String host = config.getSmtpHost();
    Properties props = new Properties();
    props.put("mail.smtp.host", host);
    props.put("mail.smtp.port", config.getSmtpPort());
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    // Get the Session object.
    Session session = Session.getInstance(props,
        new javax.mail.Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
          }
        });

    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(from));
      message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to));

      // Set Subject: header field
      message.setSubject(SUBJECT);

      Multipart multipart = new MimeMultipart();

      MimeBodyPart textBodyPart = new MimeBodyPart();
      textBodyPart.setText(MSG);

      MimeBodyPart messageBodyPart = new MimeBodyPart();
      DataSource source = new FileDataSource(zipFile);
      messageBodyPart.setDataHandler(new DataHandler(source));
      messageBodyPart.setFileName(zipFile.getName());
      multipart.addBodyPart(messageBodyPart);

      multipart.addBodyPart(textBodyPart);
      message.setContent(multipart);
      Transport.send(message);
      log.info("Sent message successfully to {}",sendToEmail);

    }
    catch (MessagingException e) {
      log.error("Error sending email to {} ", sendToEmail,e);
    }
  }

}
