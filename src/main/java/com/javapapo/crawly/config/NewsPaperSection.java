package com.javapapo.crawly.config;

import lombok.Data;

@Data
public class NewsPaperSection {
  private String name;
  private String link;
}
