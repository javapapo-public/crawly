package com.javapapo.crawly.config;

import java.util.List;
import lombok.Data;

@Data
public class ConfigModel {
  private List<NewsPaper> newspapers;
  private EmailConfig email;
}
