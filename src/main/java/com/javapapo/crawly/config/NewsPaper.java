package com.javapapo.crawly.config;

import java.util.List;
import lombok.Data;

@Data
public class NewsPaper {
   private String name;
   private String type;
   private String className;
   private boolean enabled;
   private List<NewsPaperSection> sections;
}
