package com.javapapo.crawly.config;

import lombok.Data;

@Data
public class EmailConfig {
  private String smtpHost;
  private int smtpPort;

}
