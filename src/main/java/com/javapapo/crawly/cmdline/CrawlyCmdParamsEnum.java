package com.javapapo.crawly.cmdline;

/**
 * Enumeration to contain the literals used as command line arguments.
 * See <a href="https://github.com/javapapo/crawly/blob/master/docs/HOWTORUN.md">here for official documentation</a>
 * Created by javapapo on 29/12/15.
 */
public enum CrawlyCmdParamsEnum {

    help("help"),

    maxitems("maxitems"),
    zip("zip"),
    parallel("parallel"),
    proxy("proxy"),
    proxyport("proxyport"),
    basedir("basedir"),

    //langs
    greek("greek"),
    english("english"),

    /* http system proxy settings */
    httpProxyHost("http.proxyHost"),
    httpProxyPort("http.proxyPort"),

    /* */
    sendEmail("sendEmail"),
    emailTo("emailTo"),
    gmailPassword("gmailPassword"),
    gmailUsername("gmailUsername");

    private final String literal;

    CrawlyCmdParamsEnum(String aLit) {
        this.literal = aLit;
    }

    public String literal() {
        return this.literal;
    }
}
