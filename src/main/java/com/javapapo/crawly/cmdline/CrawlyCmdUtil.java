package com.javapapo.crawly.cmdline;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import lombok.extern.slf4j.Slf4j;

/**
 * Utility based on joptsimple that parses the command line arguments form the app and returns a
 * POJO with the values.
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 18/12/15.
 */
@Slf4j
public class CrawlyCmdUtil {

    private static final String baseDefaultDir = "crawlyreports";
    private static final int DEFAULT_MAX_ARTICLES = 5;

    /**
     * Parse the command line arguments with JOpt-simple
     *
     * @param args
     * @return OptionSet
     */
    public static CrawlyCmdArgs parseArguments(String... args) throws Exception {
        CrawlyCmdArgs result = null;
        OptionParser parser = new OptionParser();

        parser.accepts(CrawlyCmdParamsEnum.help.literal(), "Prints help").forHelp();
        parser.accepts(CrawlyCmdParamsEnum.maxitems.literal(),
                "Maximum number of articles to fetch for each source / site")
                .withRequiredArg().ofType(Integer.class).defaultsTo(DEFAULT_MAX_ARTICLES);
        //shall we zip?
        parser.accepts(CrawlyCmdParamsEnum.zip.literal(), "Each generated html file is zipped");
        //use executor?
        parser.accepts(CrawlyCmdParamsEnum.parallel.literal(), "Initiates multiple crawlies at the same time, " +
                "rather than crawlying each site one by one");
        //proxy/port
        parser.accepts(CrawlyCmdParamsEnum.proxy.literal(), "Define the proxy server, in case you are running crawly " +
                "behind a proxy")
                .withRequiredArg().ofType(String.class);
        parser.accepts(CrawlyCmdParamsEnum.proxyport.literal(),
                " Define the proxy port, in case you are running crawly behind a proxy")
                .withRequiredArg().ofType(String.class);
        parser.accepts(CrawlyCmdParamsEnum.basedir.literal(),
                "Specify the directory where you want your generated reports (or zips) to be saved," +
                        " it defaults to /crawlyreports ")
                .withRequiredArg().ofType(String.class).defaultsTo("crawlyreports");

        OptionSpec<Void> email = parser.accepts(CrawlyCmdParamsEnum.sendEmail.literal(), "Send the report as an email?, it "
            + "defaults to false");
        parser.accepts(CrawlyCmdParamsEnum.gmailUsername.literal(), "Your gmail account username to send the email")
            .requiredIf(CrawlyCmdParamsEnum.sendEmail.literal()).withRequiredArg().ofType(String.class);
        parser.accepts(CrawlyCmdParamsEnum.gmailPassword.literal(),"Your gmail account pwd to send the email")
            .requiredIf(CrawlyCmdParamsEnum.sendEmail.literal()).withRequiredArg().ofType(String.class);
        parser.accepts(CrawlyCmdParamsEnum.emailTo.literal(),"The recipients email address")
            .requiredIf(CrawlyCmdParamsEnum.sendEmail.literal()).withRequiredArg().ofType(String.class);


        //flags for activating group of crawlers based on the supported language.
        parser.accepts(CrawlyCmdParamsEnum.greek.literal(), "Crawl all the supported Greek newspaper / sites");
        parser.accepts(CrawlyCmdParamsEnum.english.literal(), "Crawl all the supported English newspaper / sites ");

        OptionSet options = parser.parse(args);

        if (getHelp(options)) {
            parser.printHelpOn(System.out);
        } else {
            result = CrawlyCmdArgs.builder().
                    doParallel(getParallel(options)).
                    maxItems(getMaxArticle(options)).
                    proxy(getProxy(options)).
                    performZip(getZipFlag(options)).
                    proxyPort(getProxyPort(options)).
                    baseDir(getBaseDir(options)).
                    doGreek(getDoGreek(options)).
                    doEnglish(getDoEnglish(options)).
                    sendEmail(getSendEmail(options)).
                    sendEmailTo(getEmailTo(options)).
                    gmailPassword(getGmailPassword(options)).
                    gmailUsername(getGmailUsername(options)).
                    build();
        }
        return result;
    }


    private static String getProxy(OptionSet options) {
        return (String) options.valueOf(CrawlyCmdParamsEnum.proxy.literal());
    }

    private static String getProxyPort(OptionSet options) {
        return (String) options.valueOf(CrawlyCmdParamsEnum.proxyport.literal());
    }

    private static String getBaseDir(OptionSet options) {
        String baseDir = (String) options.valueOf(CrawlyCmdParamsEnum.basedir.literal());
        if (baseDir.matches(" *[~#%&*{}/:<>?|\"-]+ *")) {
            baseDir = baseDefaultDir;
        }
        return baseDir;
    }

    private static int getMaxArticle(OptionSet options) {
        int maxArticle = (Integer) options.valueOf(CrawlyCmdParamsEnum.maxitems.literal());
        if (maxArticle < 1 || maxArticle > 10) {
            log.warn("You have provided an invalid value for max items" +
                    " (allowed values 1 to 10)" +
                    " - I will default to 5.");
            maxArticle = DEFAULT_MAX_ARTICLES;
        }
        return maxArticle;
    }

    private static boolean getHelp(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.help.literal());
    }

    private static boolean getParallel(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.parallel.literal());
    }

    private static boolean getZipFlag(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.zip.literal());
    }

    private static boolean getDoGreek(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.greek.literal());
    }

    private static boolean getDoEnglish(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.english.literal());
    }

    private static boolean getSendEmail(OptionSet options) {
        return options.has(CrawlyCmdParamsEnum.sendEmail.literal());
    }
    private static String getEmailTo(OptionSet options) {
        return (String) options.valueOf(CrawlyCmdParamsEnum.emailTo.literal());
    }
    private static String getGmailPassword(OptionSet options) {
        return (String) options.valueOf(CrawlyCmdParamsEnum.gmailPassword.literal());
    }
    private static String getGmailUsername(OptionSet options) {
        return (String) options.valueOf(CrawlyCmdParamsEnum.gmailUsername.literal());
    }
}
