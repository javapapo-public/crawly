package com.javapapo.crawly.cmdline;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * POJO to contain various command line flags for the crawly
 * Inner Builder provided.
 * Created by <a href="mailto:javapapo@mac.com">javapapo</a> on 18/12/15.
 */
@Data
@Builder
public class CrawlyCmdArgs {

    private final int maxItems;
    private final boolean performZip;

    private final boolean doParallel;
    private final String baseDir;
    private final String proxy;
    private final String proxyPort;

    //flag activating crawling greek sites
    private final boolean doGreek;
    private final boolean doEnglish;

    private final boolean sendEmail;
    private final String gmailUsername;
    private final String gmailPassword;
    private final String sendEmailTo;

}
