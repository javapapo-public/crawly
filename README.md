Crawly -  News Site Crawler
==================


<img src="https://github.com/javapapo/crawly/blob/master/crawly.jpg?raw=true" align="center" height="100" width="100" >

>A simple command line utility written in Java, based on [Jsoup](http://jsoup.org) and [RomeFetcher](http://rometools.github.io/rome-fetcher/), that crawls specific sections of news sites (basically RSS feeds), parses the latest news articles and then flushes the contents to a plain html file, removing any dynamic content or images. The generated html files are far smaller in terms of size comparing to browsing the pages using a browser. The utility is '<b>stateless</b>' meaning it does not preserve any history of previous executions, generated articles or files. It parses on the fly the news site, so you need to be 'online' in order to use it.

This started as a couple of hours (days) hack and my main motivation was to create a utility so I can send the news to my father everyday. My father is a trade navy captain, he uses satellite internet whenever he can, but it costs a lot even for simple things. Everytime he browses some greek (or other) news sites, he gets to be charged a lot per KB(s). So I needed something to get rid
of all the images & dynamic content, parse the actual text and create a simple summary html file. In the early days I used to do this manually, and it was a real pain. The news should be delivered in plain html, which is easy to read in any browser (even old versions). See related post in my blog [here](http://javapapo.blogspot.gr/2015/12/playing-with-jsoup-and-crawling-greek.html). Of course the code is tailored made  for each specific news paper/site and the CSS selectors (Functions) are specific to the way this particular site has its html DOM. In all cases I rely on the provided RSS feeds, and the sections which I parse, are predefined by me - compile time (see the code). This is a night hack, started as an excercise to kill some time between jobs and experiment with Java 8! Feel free to use it if you have similar needs. If you have any comments, or proposals for change please email me or pull the repo and start coding.

I would appreciate if you gave me back some of your improvements, my dad would be happy as well :) .

Paris Apostolopoulos (aka javapapo)
Email : `<javapapo@mac.com>`
Twitter: `@javapapo`

Requirements
------------

- In order to execute (if you are just a user), you need [Java11 in your machine. Just download the jar execute it (see below)
- In order to package and execute (if you are user and developer) then you need [Maven 3](http://maven.apache.org/) see the [how to build section](docs/HOWTOBUILD.d=md)

How to run
----------

### Quick Start

If you are a plain user, just download the final from the [//bin](https://github.com/javapapo/crawly/tree/master/bin) 
directory and then execute with default options. 


You need to specify which language type sites do you prefer. See section 5.1.

Have a look on the Command line Options sections, below - in case you want something extra.

```bash
java -jar crawlie-xx.jar -english 
```

```bash
java -jar crawlie-xx.jar -greek 
```

### Command line options

For a list of all the available options type :
```bash
java -jar crawlie-xx.jar -help 
```
 
#### language types

You need to provide a specific language identifier that will enable a specific set of crawlers, depending on the language
the crawled sites support. Caution, in case you dont define any lang type, then crawly will not parse any site!!! See, 
section 4 above, about the crawled sites, each language supports.

| language type  | param         | 
| -------------  | ------------- |
| greek          | -greek        |
| english        | -english      |

Example: 

Will activate crawling of greek news sites
```bash
java -jar crawlie-xxx.jar -greek
```

Will activate crawling of english news sites
```bash
java -jar crawlie-xxx.jar -english
```

#### maxitems (optional)
Controls the maximum amount of items (links) per section to be included in the final output file.Allowed values are from 1 to
10. In case you provide a value not within the allowed range, I will default to 5 and continue execution.

Example: 

```bash
java -jar crawlie-xxx.jar -maxitems=10
```

#### zip (optional)
Controls the creation of zip files (along with the standard html). If this argument is passed to the crawler
it will create zip files for each html. The name of the zip file is identical with the original one e.g
tanea-2015-02-21.html will have a tanea-2015-02-21.html.zip sibling.

Example: 

```bash
java -jar crawler-xxx.jar -zip
```

#### parallel (optional)
When you use this switch, the application is going to use fixed size thread executor and will run the crawlers in parallel
. The number of parallel threads is dependend on the capabilities of your CPU. The default mode for the crawler is non
parallel, where all the executors run one after another.

Example: 

```bash
java -jar crawlie-xxx.jar -parallel
```

#### proxy & proxyport (optional)
In case you operate through a proxy, then the crawler needs to now the IP and port.

Example: 

```bash
java -jar crawlie-xxx.jar -proxy=192.168.1.1 -proxyport=8080
```

#### basedir (optional)
In case you provide the basedir value, crawly will create a new folder, relative to the current folder where you run crawly, and it will output the generated files. In case you dont define basedir the default value is 'crawlyreports'. In case you provide
any invalid values in the property e.g %^&*#$^ , crawly will just ignore them and will switch to default!

Example: 

```bash
java -jar crawlie-xxx.jar -basedir=testreports
```


If you execute crawly from C:\ , then crawly will create C:\testreports and then it will output the generated files

#### sendEmail (optional)
Crawly can send the generated report to your email of choice. In order to do that `Crawly` requires a `Gmail` account
so that it can use to send the email. Usually that would be your personal account.

So,If you add `-sendEmai` then you need to provide the following 3 extra arguments

```text
-sendEmail -gmailUsername <YourGmailAccountUsername> -gmailPassword <YourGmailAccountPwd> -emailTo <WhereToSendTheReport>
```
Example:

```text
java -jar crawlie-xxx.jar -greek -sendEmail -gmailUsername javaneze -gmailPassword ay3xygh123 -emailTo dimitris@mac.com
```


Versions / Features
-------------------


| version        | Fixes         | 
| -------------  | ------------- |
| 1.0            | Basic Crawler only for the newspaper tovima.gr        |
| 1.1            | Add an extra crawler for the newspaper protothema.gr,add some simple logic to get only the most recent items |
| 1.2            | Add JOpts to support command line arguments, add maxitems optional command line.|
| 1.3            | Add Rome Fetcher lib to support parsing XML feeds (when available), Added Naftemporiki news paper | 
| 1.4            | Sport24 sports site |
| 1.5            | TaNea.gr newspaper added + small improvements on the code |
| 1.6            | Kathimerini was added.All the existing implementations now rely on RSS feeds, rather than DOM parsing of section pages |
| 1.7            | Add in.gr |
| 1.8            | Added sites PressProject/Capital.gr/Contra.gr. Also added the -zip command line argument, the crawler can now zip the html reports |
| 1.9            | Add -parallel switch, to run crawlers in parallel instead of the standard sequential execution.|
| 2.0            | Add proxy/port (no authentication) and package refactoring |
| 2.1            | Add protagon.gr / change name of project |
| 2.2            | Add news247.gr  only include feeds that were posted the day you run crawly |
| 2.3            | Add gazzetta.gr / minor fixes |
| 2.4            | Add parapolitika.gr - add folder for generated files |
| 2.5            | Add lang types command line param, add reuters, fixes |
| 2.6            | Add new york times, fixes on some greek sites, small changes to generated html |
| 2.7            | Add 2 german sites, removed a greek sports one (gazzetta) |
| 2.8            | Fixes on retrieving only the current date article |
| 2.9            | Add skai.gr on the greek sites section |
| 2.9.1          | Refactoring of html content writer |
| 2.9.2          | fix for news247.gr |
| 3.0            | refactoring and drop the gui module |
| 3.1            | Add --help option for help menu, add lombok |
| 3.2            | After many years make fixes- remove some dead newspapers |
| 3.3            | Fix the old code, introduce config, drop non greek newspapers |
| 3.4            | Send email automatically |

 
How to build
------------

Execute : 

```
mvn clean package 
```

This will create a crawlie-XX.jar (it has all the required dependencies)

There is also a helper bash script (runner.sh) that executes the packaging and then runs the jar. Make sure you chmod it, in order to make it executable.

```bash
./runner.sh
```


Generated Files
------------

When you execute the crawler, you are expected to html files generated a folder relateive to where you execute the jar 
the default folder that will be generated is /crawlyreports (you can provide your own folder using the -basedir option).

```bash
./crawlyreports/[name_of_the_newspaper]-date--localime.html
```
Example : 

```bash
./crawlyreports/ΠρώτοΘέμα-2015-12-14--19:30:40.html
```

```bash
Τοcrawlyreports/Βήμα-2015-12-14--19:30:40.html
```

Crawled sites
-------------

| Language       | Supported sites         | 
| -------------  | ----------------------- |
| greek   | [ΤοΒήμα](http://www.tovima.gr),[Πρώτοθεμα](http://www.protothema.gr), [Ναυτεμπορική](http://www.naftemporiki.gr), [Sport24](http://www.sports24.gr), [ΤαΝέα](http://www.tanea.gr), [Καθημερινή](http://www.kathimerini.gr), [In.gr](http://www.in.gr), [Capital.gr](http://www.capital.gr), [Contra.gr](http://www.contra.gr), [Gazzetta.gr](http://www.Gazzetta.gr), [ThePressProject](http://www.thepressproject.gr), [protagon.gr](http://www.protagon.gr), [parapolitika.gr](http://www.parapolitika.gr), [realnews](http://www.realnews.gr),[skai.gr](http://www.skai.gr) |
| english | [reuters](http://www.reuters.com), [bbc](http://www.bbc.com), [NewYorkTimes](http://www.nytimes.com), [Reuters](http://www.reuters.com) |
| german  | [spiegel](http://www.spiegel.de), [frankfurter allgemeine](http://www.faz.net) |


RSS feeds for each site
-----------------------
Currently the supported RSS feeds for each site, are handpicked by me. Crawly uses a set of property files, that are bundled
within the code, which indicate all the links of feeds per site. If you want to have a look and examine these files please have
a look [here](https://gitlab.com/javapapo/crawly/tree/master/crawly/src/main/resources). 

Feel free to email me for any particular change (addition or removal of feeds). Of course if you fork Crawly, you can change the contents
of the property files, adding your own mix of feeds.

